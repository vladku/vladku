[![pipi](https://img.shields.io/static/v1?label=pypi&message=vladku&color=blue&style=for-the-badge)](https://pypi.org/user/vladku/)
[![codewar](https://www.codewars.com/users/vladku/badges/large)](https://www.codewars.com/users/vladku)

### Hi I am Vlad

I'm a software engineer who is passionate about software development.

## Languages and Tools
<a href="https://www.postgresql.org">
    <img align="left" width="28px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/29/Postgresql_elephant.svg/233px-Postgresql_elephant.svg.png"/>
</a>
<a href="https://code.visualstudio.com">
    <img alt="Visual Studio Code" align="left" width="28px" style="margin-right:15px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/visual-studio-code/visual-studio-code.png" />
</a>
<a href="https://www.python.org">
    <img alt="Python" align="left" width="32px" style="margin-right:15px" src="https://raw.githubusercontent.com/github/explore/80688e429a7d4ef2fca1e82350fe8e3517d3494d/topics/python/python.png" />
</a>
<a href="https://clojure.org">
    <img align="left" width="28px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5d/Clojure_logo.svg/256px-Clojure_logo.svg.png"/>
</a>
<a href="https://go.dev">
    <img align="left" width="28px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Go_gopher_favicon.svg/480px-Go_gopher_favicon.svg.png"/>
</a>
<a href="https://www.rust-lang.org">
    <img align="left" width="40px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/Rustacean-flat-happy.svg/320px-Rustacean-flat-happy.svg.png"/>
</a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">
    <img align="left" width="20px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Badge_js-strict.svg/173px-Badge_js-strict.svg.png"/>
</a>
<a href="https://css-tricks.com">
    <img align="left" width="20px" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/CSS.3.svg/171px-CSS.3.svg.png"/>
</a>
